import { useContext } from "react";
import { ThemeContext } from "../context/ThemeContext";
import { createGlobalStyle } from "styled-components";

import spaceImg from "../assets/space_image.jpg";

const themes = {
    light: {
        backgroundColor: "white",
        backgroundImage: null,
        color: "black",
    },
    dark: {
        backgroundColor: "black",
        backgroundImage: null,
        color: "white",
    },
    space:{
        backgroundColor: null,
        backgroundImage: `url(${spaceImg})`,
        color: "#E8EAF6",
    }
};

const StyledGlobalStyle = createGlobalStyle`
body {
    background-color: ${(props) => themes[props.theme].backgroundColor};
    color: ${(props) => themes[props.theme].color};
    background-image: ${(props) => themes[props.theme].backgroundImage};
}
.accordion-button {
    background-color: ${(props) => themes[props.theme].backgroundColor};
    background-image: ${(props) => themes[props.theme].backgroundImage};
    color: ${(props) => themes[props.theme].color};
    font-size: 30px;
    font-family: "Gill Sans", sans-serif;
}
.accordion-collapse {
    background-color: ${(props) => themes[props.theme].backgroundColor};
    background-image: ${(props) => themes[props.theme].backgroundImage};
}

.list-group-item {
    background-color: ${(props) => themes[props.theme].backgroundColor};
    background-image: ${(props) => themes[props.theme].backgroundImage};
    color: ${(props) => themes[props.theme].color};
    font-size: 15px;
}

.btn-outline-light {
    background-color: ${(props) => themes[props.theme].backgroundColor};
    color: ${(props) => themes[props.theme].color};
    height: 30px;
    font-size: 12px;
    padding: 2px 6px 2px 6px;
    display: flex;
    margin-bottom: 5px;
}

.tooltip {
    background-color: ${(props) => themes[props.theme].backgroundColor};
    color: ${(props) => themes[props.theme].color};
    background-image: ${(props) => themes[props.theme].backgroundImage};
    height: 30px;
    font-size: 12px;
}
`

function GlobalStyle() {

    const { theme } = useContext(ThemeContext);

    return (
        <StyledGlobalStyle 
            theme={theme}
        />
    )
}

export default GlobalStyle;

