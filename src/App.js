import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";

import { ThemeProvider } from "./context/ThemeContext";
import { AxiosProvider } from "./context/AxiosContext";

import Home from "./pages/Home";
import StarlinkList from "./pages/StarlinkList";
import StarlinkMap from "./pages/StarlinkMap";

import GlobalStyle from "./styles/GlobalStyle";

function App() {

  return (
    <ThemeProvider >
      <AxiosProvider>
        <GlobalStyle />
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<Home />}/>
            <Route path="/starlink-list" element={<StarlinkList />}/>
            <Route path="/starlink-map" element={<StarlinkMap />}/>         
          </Routes> 
        </BrowserRouter>
             
      </AxiosProvider>
    </ThemeProvider>
  );
}

export default App;
