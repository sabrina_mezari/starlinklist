import React, {useContext} from "react";
import { ComposableMap, Geographies, Geography, Graticule, Marker } from "react-simple-maps";

import { AxiosContext } from "../context/AxiosContext";
import "../styles/starlinkMap.css";

import Navigation from "../components/Navigation";


const geoUrl =
    "https://raw.githubusercontent.com/zcreativelabs/react-simple-maps/master/topojson-maps/world-110m.json";

function SimpleMap() {

    const {datas} = useContext(AxiosContext);

    return (
        <div>
            <Navigation />
            <ComposableMap projectionConfig={{ scale: 147 }}>
                <Graticule stroke="#F53" />
                <Geographies geography={geoUrl}>
                {({ geographies }) =>
                    geographies.map(geo => <Geography key={geo.rsmKey} geography={geo} />)
                }
                </Geographies>
                {datas.map((satellite) => (
                    <Marker coordinates={[satellite.longitude, satellite.latitude]} key={satellite.id}>
                    <circle r={1.5} fill="#F53" />
                    </Marker>
                ))}    
            </ComposableMap>
        </div>
    )
}

export default SimpleMap;