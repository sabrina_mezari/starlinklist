import Navigation from "../components/Navigation";

import "../styles/home.css";


function Home() {
    return(
        <div>
            <Navigation /> 
            <div className="fade"></div>
            <section className="star-wars">
                <div className="crawl">
                    <div className="title">
                        <p>Episode I</p>
                        <h1>Starlink</h1>
                    </div>     
                    <p>Starlink est une constellation de satellites permettant l'accès à Internet par satellite, déployée et gérée par le constructeur aérospatial américain SpaceX.</p>
                    <p>Elle repose sur plusieurs milliers de satellites de télécommunications placés sur une orbite terrestre basse.</p>  
                </div>
            </section>
            
        </div>
    )
}

export default Home;