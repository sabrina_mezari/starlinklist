import 'bootstrap/dist/css/bootstrap.min.css';
import {OverlayTrigger, Button, Tooltip} from "react-bootstrap";

function StarlinkCriteria({ spaceTrack, longitude, latitude, height_km, velocity_kms}) {

    return (
        <div >
            {['right'].map((placement) => (
                <OverlayTrigger
                key={placement}
                placement={placement}
                overlay={
                    <Tooltip id={`tooltip-${placement}`}>
                            {spaceTrack.LAUNCH_DATE === null ? (<small>pas de données</small>) : (<p>{spaceTrack.LAUNCH_DATE}</p>)}                        
                    </Tooltip>  
                }
                    >
                <Button variant="outline-light">Date de lancement</Button>
                </OverlayTrigger>
            ))}

            {['right'].map((placement) => (
                <OverlayTrigger
                key={placement}
                placement={placement}
                overlay={
                    <Tooltip id={`tooltip-${placement}`}>
                            {longitude === null ? (<small>pas de données</small>) : (<p>{longitude}</p>)} 
                    </Tooltip>  
                }
                    >
                <Button variant="outline-light">Longitude</Button>
                </OverlayTrigger>
            ))}

            {['right'].map((placement) => (
                <OverlayTrigger
                key={placement}
                placement={placement}
                overlay={
                    <Tooltip id={`tooltip-${placement}`}>
                            {latitude === null ? (<small>pas de données</small>) : (<p>{latitude}</p>)}                        
                    </Tooltip>  
                }
                    >
                <Button variant="outline-light">Latitude</Button>
                </OverlayTrigger>
            ))}

            {['right'].map((placement) => (
                <OverlayTrigger
                key={placement}
                placement={placement}
                overlay={
                    <Tooltip id={`tooltip-${placement}`}>
                            {height_km === null ? (<small>pas de données</small>) : (<p>{height_km}</p>)}                        
                    </Tooltip>  
                }
                    >
                <Button variant="outline-light">Hauteur en km</Button>
                </OverlayTrigger>
            ))}

            {['right'].map((placement) => (
                <OverlayTrigger
                key={placement}
                placement={placement}
                overlay={
                    <Tooltip id={`tooltip-${placement}`}>
                            {velocity_kms === null ? (<small>pas de données</small>) : (<p>{velocity_kms}</p>)} 
                    </Tooltip>  
                }
                    >
                <Button variant="outline-light">Rapidité en km/s</Button>
                </OverlayTrigger>
            ))}
            
        </div>
    )
}

export default StarlinkCriteria;