import { useState, useEffect } from "react";
import axios from "axios";

import { Accordion } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import "../styles/starlinkList.css";

import StarlinkCriteria from "./StarlinkCriteria";
import Loader from "../utils/loader";
import FormSort from "../components/FormSort";
import Navigation from "../components/Navigation";



function StarlinkList() {

    const [starlinkList, setStarlinkList] = useState([]);
    const [isDataLoading, setDataLoading] = useState(false);


    useEffect(() => {
        async function fetchStarlinkList() {
    
            setDataLoading(true);

            const response = await axios.get("https://api.spacexdata.com/v4/starlink");
            setStarlinkList(response.data);

            setDataLoading(false);
        }
        fetchStarlinkList();
    }, []) 


    function sortByLaunchDate() {
        const copyOfStarlinkList = [...starlinkList];
        copyOfStarlinkList.sort((a,b) => {
            return new Date(b.spaceTrack.LAUNCH_DATE) - new Date(a.spaceTrack.LAUNCH_DATE)
        })
        setStarlinkList(copyOfStarlinkList);
    }

    function sortByHeight() {
        const copyOfStarlinkList = [...starlinkList];
        copyOfStarlinkList.sort((a,b) => {
            return b.height_km - a.height_km
        })
        setStarlinkList(copyOfStarlinkList);
    }

    function sortByVelocity() {
        const copyOfStarlinkList = [...starlinkList];
        copyOfStarlinkList.sort((a,b) => {
            return b.velocity_kms - a.velocity_kms
        })
        setStarlinkList(copyOfStarlinkList);
    }

    

    return(
        <div>
            {isDataLoading ? (
                <Loader />    
            ) : (
                <div>
                    <Navigation />
                    <FormSort 
                        sortByLaunchDate={sortByLaunchDate}
                        sortByHeight={sortByHeight}
                        sortByVelocity={sortByVelocity}
                    />

                    <Accordion className="accordion-starlink-list" >
                        {starlinkList.map(({ id, spaceTrack, longitude, latitude, height_km, velocity_kms }) => (
                            <Accordion.Item className="accordion-item" eventKey={id} key={id}>
                                <Accordion.Header>
                                    {spaceTrack.OBJECT_NAME}
                                </Accordion.Header>
                                    <Accordion.Body >
                                        <StarlinkCriteria
                                            id={id}
                                            spaceTrack={spaceTrack}
                                            longitude={longitude}
                                            latitude={latitude}
                                            height_km={height_km}
                                            velocity_kms={velocity_kms}
                                        />
                                    </Accordion.Body>
                            </Accordion.Item>
                        ))}
                    </Accordion>
                </div>
            )}   
        </div>
    )
}

export default StarlinkList;