import starlinkGif from "../assets/starlink-satellites.gif";
import "../styles/loader.css";


function Loader() {
    return(
        <div className="starlink-gif">
            <img src={starlinkGif} alt="satellites-starlink-gif"/>
        </div>
    )
}

export default Loader;