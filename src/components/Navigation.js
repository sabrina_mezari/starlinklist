import { Container, Nav, NavDropdown, Navbar } from "react-bootstrap";

import ModeStyle from "./ModeStyle";



function Navigation() {
    return (
        <div>
            <Navbar bg="dark" variant="dark">
                <Container>
                    <Navbar.Brand>Les satellites Starlink</Navbar.Brand>
                    <Nav className="me-auto">
                        <Nav.Link href="/">Home</Nav.Link>
                        <Nav.Link href="/starlink-list">Liste </Nav.Link>
                        <Nav.Link href="/starlink-map">Map</Nav.Link>
                    </Nav>
                    <NavDropdown title="Changer de mode" id="collasible-nav-dropdown">
                        <NavDropdown.Item style={{backgroundColor:"black"}}> <ModeStyle /> </NavDropdown.Item>
                    </NavDropdown>
                </Container>
            </Navbar>
                    
        </div>
    )
}

export default Navigation;