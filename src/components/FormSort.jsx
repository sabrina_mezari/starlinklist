import { Button } from "react-bootstrap";
import "../styles/formSort.css";

function FormSort({ sortByLaunchDate, sortByHeight, sortByVelocity }) {

    return (
        <div className="button-form-sort">
            <Button variant="outline-secondary" onClick={sortByLaunchDate}>Clique pour trier de la date de lancement la plus récente à la plus ancienne</Button>
            <Button variant="outline-secondary" onClick={sortByHeight}>Clique pour trier les satellites du plus haut au plus bas</Button>
            <Button variant="outline-secondary" onClick={sortByVelocity}>Clique pour trier les satellites du plus rapide au moins rapide</Button>
        </div>
    )
}

export default FormSort;