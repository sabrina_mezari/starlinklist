import { useContext } from "react";

import styled from "styled-components";
import { ThemeContext } from "../context/ThemeContext";


const DivGroup = styled.div`
    display: flex;
    justify-content: space-around;
    font-size: 25px;
    cursor: pointer;
`

function ModeStyle() {

    const { setTheme } = useContext(ThemeContext);

    return(
        <div>
            
            <DivGroup>
                <div onClick={() => setTheme("light")}>🌞</div>
                <div onClick={() => setTheme("dark")}>🌙</div>
                <div onClick={() => setTheme("space")}>🌟</div>
            </DivGroup>
        </div>
    )
}

export default ModeStyle;