import axios from "axios";
import React, { createContext, useEffect, useState } from "react";

export const AxiosContext = createContext();

export const AxiosProvider = ({children}) => {

    const [datas, setDatas] = useState([]);

    useEffect(() => {
        const fetchData = async () => {
        const response = await axios.get("https://api.spacexdata.com/v4/starlink");
        setDatas(response.data);
        };
        fetchData();
    }, [])
    

    return (
        <AxiosContext.Provider value= {{datas}}>
            {children}
        </AxiosContext.Provider>
    )
}